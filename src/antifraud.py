
#!/usr/bin/python

import sys, getopt
import networkx as nx
import matplotlib.pyplot as plt


def main(argv):

	batch_file = sys.argv[1]
	stream_file = sys.argv[2]
	output1 = sys.argv[3]
	output2 = sys.argv[4]
	output3 = sys.argv[5]

	# Defien a new graph
	G = nx.Graph()
     
    # Buid in the graph
	with open(batch_file, "r") as f:
	    lines = f.readlines()[1:]
	    print(len(lines))
	    i = 0
	    for line in lines:
	   		node = line.split(",")
	   		# i =i+1
	   		# print(i, node[1].strip(),node[2].strip())
	   		G.add_edge(node[1].strip(),node[2].strip())
	# print(list(G.nodes()))
	print("The graph is built.")

	out1 = open(output1, "w")
	out2 = open(output2, "w")
	out3 = open(output3, "w")
	i = 0

	# Receive stream requsts
	with open(stream_file, "r") as f:
	    lines = f.readlines()[1:]
	    for line in lines:
	   		node = line.split(",")
	   		x = node[1].strip()
	   		y = node[2].strip()
	   		i = i+1
	   		print(i)
	   		if (G.has_node(x) and G.has_node(y)):
	   			# Feature 1
	   			if G.has_edge(x, y):
	   				out1.write("trusted\n")	 
	   			else:
		   			out1.write("unverified\n")
		   			# Feature 2
		   			if list(nx.all_simple_paths(G, x, y, cutoff=2)):
		   				out2.write("trusted\n")
		   				# Feature 3
		   				if list(nx.all_simple_paths(G, x, y, cutoff=4)):
			   				out3.write("trusted\n")
			   			else:
						   	out3.write("unverified\n")
		   			else:
					   	out2.write("unverified\n")	
			else:
				print("Node not in graph")
		   		out1.write("unverified\n")
		   		out2.write("unverified\n")
		   		out3.write("unverified\n")
	   		

	out1.close()
	out2.close()
	out3.close()

	# G = nx.erdos_renyi_graph(20,0.08)
	# nx.draw(G,with_labels = True)
	# plt.show()
	print("Output is ready.")



if __name__ == "__main__":
   main(sys.argv[5:])


